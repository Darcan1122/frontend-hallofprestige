/* import Nav from "./components/Nav"; */
import Radio from "./components/Radio";
import styles from "./components/HallForm.module.css";

export default function HallOfPrestige() {
  return (
    <div className={styles.container}>
      {/* <Nav /> */}
      <div className={styles.content}>
        <main className={styles.mainContent}>
          <h1 className={styles.title}>Welcome!</h1>
          <h3 className={styles.subtitle}>
            Join the Riviera Montecarlo Hall of Prestige
          </h3>
          <p className={styles.intro}>
            The hall of prestige includes distinguished personalities that
            acquired unique art pieces and Grace models. As a Grace owner you
            will be able to exhibit your art, your Grace and share your vision
            in the Hall of Prestige as part of this distinguished community.
          </p>
          <p className={styles.question}>
            Would you like to be part of the Hall of Prestige?
          </p>
          <Radio />
        </main>
      </div>
    </div>
  );
}
