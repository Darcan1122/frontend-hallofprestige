import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { useState } from "react";
import FileUploader from "./FileUploader";

const theme = createTheme({
  palette: {
    success: {
      main: "#2F4C30",
    },
  },
});

export default function CheckboxLabel() {
  const [checked, setChecked] = useState(false);

  const handleChange2 = (event) => {
    setChecked([event.target.checked, checked[1]]);
  };

  const handleChange3 = (event) => {
    setChecked([checked[0], event.target.checked]);
  };

  return (
    <>
      <ThemeProvider theme={theme}>
        <FormGroup>
          <FormControlLabel
            control={
              <Checkbox
                color="success"
                checked={checked[0]}
                onChange={handleChange2}
              />
            }
            sx={{ "& .MuiSvgIcon-root": { fontSize: 28 } }}
            label="Grace"
          />
          <FormControlLabel
            control={
              <Checkbox
                color="success"
                checked={checked[1]}
                onChange={handleChange3}
              />
            }
            sx={{ "& .MuiSvgIcon-root": { fontSize: 28 } }}
            label="Art pieces"
          />
        </FormGroup>
      </ThemeProvider>
      {checked[0] && (
        <FileUploader
          text={"Add Grace"}
          onFileSelectSuccess={(file) => setSelectedFile(file)}
          onFileSelectError={({ error }) => alert(error)}
        />
      )}
      {checked[1] && (
        <FileUploader
          text={"Add art pieces"}
          onFileSelectSuccess={(file) => setSelectedFile(file)}
          onFileSelectError={({ error }) => alert(error)}
        />
      )}
    </>
  );
}
