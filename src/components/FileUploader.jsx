import React, { useRef } from "react";
/* import importIcon from "../assets/importIcon.svg";
import pencil from "../assets/pencil.svg"; */
import { ImportIcon } from "./ImportIcon";
import styles from "./HallForm.module.css";

export default function FileUploader({ onFileSelect, text }) {
  const fileInput = useRef(null);

  const handleFileInput = (e) => {
    const file = e.target.files[0];
    if (file.size > 1024)
      onFileSelectError({ error: "File size cannot exceed more than 1MB" });
    else onFileSelectSuccess(file);
  };

  return (
    <div className="file-uploader">
      <label for="file-input" className={styles.label}>
        <ImportIcon text={text} />
        <input id="file-input" type="file" onChange={handleFileInput} />
        <button
          onClick={(e) => fileInput.current && fileInput.current.click()}
        />
      </label>
    </div>
  );
}
