import CheckboxLabel from "./CheckboxLabel";
import PrimaryButton from "./PrimaryButton";
import styles from "./HallForm.module.css";
import FileUploader from "./FileUploader";
import { useState } from "react";
import carImg from "../assets/whiteCar.svg";

export default function HallForm() {
  const [inputs, setInputs] = useState({});
  const [textareas, setTextareas] = useState("");

  const handleChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    setInputs((values) => ({ ...values, [name]: value }));
  };
  const handleText = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    setTextareas((values) => ({ ...values, [name]: value }));
  };
  return (
    <form>
      <p className={styles.question}>
        Under which alias would you like to appear in the Riviera Monte Carlo
        Hall of Prestige?
      </p>
      <label>
        Name or alias
        <input
          className={styles.input}
          type="text"
          name="username"
          placeholder="Please enter a name or alias"
          value={inputs.username || ""}
          onChange={handleChange}
        ></input>
      </label>
      <p className={styles.question}>Select your profile photo</p>
      <FileUploader
        text={"Add photo"}
        onFileSelectSuccess={(file) => setSelectedFile(file)}
        onFileSelectError={({ error }) => alert(error)}
      />
      <p className={styles.question}>
        Which products would you like to display in the Riviera Montecarlo Hall
        of Prestige?
      </p>
      <CheckboxLabel />
      <p className={styles.question}>
        Is there anything you would like to share with Riviera Montecarlo
        Society and the rest of the world?
      </p>
      <textarea
        className={styles.textarea}
        placeholder="Please share your message here"
        name="description"
        value={textareas.description || ""}
        onChange={handleText}
      ></textarea>
      <PrimaryButton
        width={362}
        height={64}
        color={"#fff"}
        backgroundColor={"#5F7C66"}
        border={"none"}
        buttonText={"Confirm"}
        fontSize={18}
      />
      <img src={carImg} alt="car" className={styles.carImg} />
    </form>
  );
}
