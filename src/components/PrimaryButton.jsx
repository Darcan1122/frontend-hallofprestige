import styles from "./PrimaryButton.module.css";

export default function PrimaryButton({
  width,
  height,
  backgroundColor,
  color,
  border,
  borderColor,
  fontSize,
  buttonText,
}) {
  return (
    <div className={styles.button}>
      <button
        className="button"
        style={{
          width,
          height,
          backgroundColor,
          color,
          border,
          borderColor,
          fontSize,
        }}
      >
        {buttonText}
      </button>
    </div>
  );
}
