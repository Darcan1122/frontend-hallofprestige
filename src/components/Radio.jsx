import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { useState } from "react";
import HallForm from "./HallForm";

const theme = createTheme({
  palette: {
    success: {
      main: "#2F4C30",
    },
  },
});

export default function Radio() {
  const [selectedValue, setSelectedValue] = useState();

  const handleChange = (event) => {
    setSelectedValue(event.target.value);
  };

  const controlProps = (item) => ({
    checked: selectedValue === item,
    onChange: handleChange,
    value: item,
    name: "radio-chb",
    inputProps: { "aria-label": item },
  });
  return (
    <>
      <ThemeProvider theme={theme}>
        <FormGroup aria-label="position" row>
          <FormControlLabel
            {...controlProps("b")}
            control={<Checkbox color="success" />}
            sx={{ "& .MuiSvgIcon-root": { fontSize: 28 } }}
            label="Yes"
          />

          <FormControlLabel
            {...controlProps("a")}
            color="success"
            control={<Checkbox color="success" />}
            sx={{ "& .MuiSvgIcon-root": { fontSize: 28 } }}
            label="No"
          />
        </FormGroup>
      </ThemeProvider>
      {selectedValue === "b" ? <HallForm /> : null}
    </>
  );
}
