import importIcon from "../assets/importIcon.svg";
import pencil from "../assets/pencil.svg";
import styles from "./Icon.module.css";

export const ImportIcon = ({ text }) => {
  return (
    <div className={styles.container}>
      <img src={importIcon} alt="import" />
      <span className={styles.span}>{text}</span>
      <img src={pencil} alt="edit" />
    </div>
  );
};
