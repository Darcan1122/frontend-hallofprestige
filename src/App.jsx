import { BrowserRouter, Routes, Route } from "react-router-dom";
import HallOfPrestige from "./HallOfPrestige";
import "./App.css";

function App() {
  return (
    <main>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<HallOfPrestige />} />
        </Routes>
      </BrowserRouter>
    </main>
  );
}

export default App;
